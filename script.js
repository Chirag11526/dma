//const api_url = "<heroku_app_url>"
const api_url = "https://esdcurd.herokuapp.com/user-details"

function loadData(records = []) {
	var table_data = "";
	for(let i=0; i<records.length; i++) {
		table_data += `<tr>`;
		table_data += `<td>${records[i].flight_id}</td>`;
		table_data += `<td>${records[i].name}</td>`;
		table_data += `<td>${records[i].age}</td>`;
		table_data += `<td>${records[i].phone}</td>`;
		table_data += `<td>${records[i].class_name}</td>`;
		table_data += `<td>${records[i].num_of_tick}</td>`;
		table_data += `<td>${records[i].price}</td>`;
		table_data += `<td>${records[i].booked_on}</td>`;
		table_data += `<td>`;
		table_data += `<a href="edit.html?id=${records[i]._id}"><button class="btn btn-primary">Edit</button></a>`;
		table_data += '&nbsp;&nbsp;';
		table_data += `<button class="btn btn-danger" onclick=deleteData('${records[i]._id}')>Delete</button>`;
		table_data += `</td>`;
		table_data += `</tr>`;
	}
	//console.log(table_data);
	document.getElementById("tbody").innerHTML = table_data;
}

function getData() {
	fetch(api_url)
	.then((response) => response.json())
	.then((data) => { 
		console.table(data); 
		loadData(data);
	});
}


function getDataById(id) {
	fetch(`${api_url}/${id}`)
	.then((response) => response.json())
	.then((data) => { 
	
		console.log(data);
		document.getElementById("id").value = data._id;
		document.getElementById("flight_id").value = data.flight_id;
		document.getElementById("name").value = data.name;
		document.getElementById("age").value = data.age;
		document.getElementById("phone").value = data.phone;
		document.getElementById("class_name").value = data.class_name;
		document.getElementById("num_of_tick").value = data.num_of_tick;
		document.getElementById("price").value = data.price;
	})
}


function postData() {
	var flight_id = document.getElementById("flight_id").value;
	var name = document.getElementById("name").value;
	var phone = document.getElementById("phone").value;
	var age = document.getElementById("age").value;
	var class_name = document.getElementById("class_name").value;
	var num_of_tick = document.getElementById("num_of_tick").value;
	var price = document.getElementById("price").value;
	
	data = {flight_id:flight_id,  name: name,  age: age,phone:phone, class_name: class_name,num_of_tick:num_of_tick,price:price };
	
	fetch(api_url, {
		method: "POST",
		headers: {
		  'Accept': 'application/json',
		  'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	})
	.then((response) => response.json())
	.then((data) => { 
		console.log(data); 
		window.location.href = "index.html";
	})
}	


function putData() {
    var _id = document.getElementById("id").value;
	var flight_id = document.getElementById("flight_id").value;
	var name = document.getElementById("name").value;
	var phone = document.getElementById("phone").value;
	var age = document.getElementById("age").value;
	var class_name = document.getElementById("class_name").value;
	var num_of_tick = document.getElementById("num_of_tick").value;
	var price = document.getElementById("price").value;
	
	data = {_id: _id,flight_id:flight_id,  name: name,  age: age,phone:phone, class_name: class_name,num_of_tick:num_of_tick,price:price };
	
	fetch(api_url, {
		method: "PUT",
		headers: {
		  'Accept': 'application/json',
		  'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	})
	.then((response) => response.json())
	.then((data) => { 
		console.table(data);
		window.location.href = "index.html";
	})
}


function deleteData(id) {
	user_input = confirm("Are you sure you want to delete this record?");
	if(user_input) {
		fetch(api_url, {
			method: "DELETE",
			headers: {
			  'Accept': 'application/json',
			  'Content-Type': 'application/json'
			},
			body: JSON.stringify({"_id": id})
		})
		.then((response) => response.json())
		.then((data) => { 
			console.log(data); 
			window.location.reload();
		})
	}
}